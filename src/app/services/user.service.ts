import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserModel } from '../Models/UserModel';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  urlBase: String = 'http://localhost:64004/api/user';

  constructor(private http: HttpClient) { }

  getUserName() {
    this.http.get(`${this.urlBase}`).subscribe(x => {
      return x;
    });
  }

  addUser(pUser: UserModel) {
    this.http.post(`${this.urlBase}/AddUser`, pUser).subscribe( x => {
      console.log(x);
    });
  }
}
