import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { UserModel } from '../../Models/UserModel';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  formData: FormGroup;
  flag: Boolean = false;

  constructor(private Userapi: UserService,
              private fb: FormBuilder) { }

  ngOnInit() {
    // Initialise notre Frmulaire
    this.formData = this.fb.group({
      email: [''], // valeur par default vide
      prenom: [''],
      nom: [''],
      date: [''],
    });
  }

  saveData(formValues: any) {
    const user = new UserModel();
    user.prenom = formValues.prenom;
    user.nom = formValues.nom;
    user.email = formValues.email;
    user.date = new Date(formValues.date.year, formValues.date.month, formValues.date.day);
    this.Userapi.addUser(user);
  }

}
