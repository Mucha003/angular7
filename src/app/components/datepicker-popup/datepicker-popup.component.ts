import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-datepicker-popup',
  templateUrl: './datepicker-popup.component.html',
  styleUrls: ['./datepicker-popup.component.css']
})
export class DatepickerPopupComponent {

  dateChoisi;
  @Input() DateForm: FormGroup;
  @Input() NameFormControl: String;
  @Input() LabelForm: String;

  constructor() { }
}
